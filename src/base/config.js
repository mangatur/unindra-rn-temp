export const config = {
	apiUrl: 'https://enigmatic-spire-94540.herokuapp.com/8/api',
	headers: {
		'x-access-token':
			'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyTmFtZSI6ImFkbWluIiwic3VmZml4IjoiOCIsImlhdCI6MTYzMDkzOTc2NCwiZXhwIjoxNjMxMDI2MTY0fQ.bPAdWNiGJi0YpYqBDqXp2pTmp0O5Rr7eHVjHb0ZiePE',
	},
};
