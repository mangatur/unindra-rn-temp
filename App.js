import * as React from 'react';
import { Text, View, StyleSheet } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

import Home from './src/components/home';
import Category from './src/components/category';

function HomeScreen() {
	return (
		<View style={{ padding: 10 }}>
			<Text style={{ fontSize: 24 }}>Welcome,</Text>
			<Home nama="Budi" styles={styles} />
		</View>
	);
}

function SettingsScreen() {
	return (
		<View style={{ padding: 10 }}>
			<Text>Settings!</Text>
		</View>
	);
}

const Tab = createBottomTabNavigator();

const styles = StyleSheet.create({
	container: {
		margin: 10,
		flex: 1,
		flexDirection: 'row',
		flexWrap: 'wrap',
		alignItems: 'flex-start',
	},
	item: {
		width: '50%',
	},
	bigBlue: {
		color: 'blue',
		fontWeight: 'bold',
		fontSize: 30,
	},
	title: {
		fontSize: 24,
		color: 'blue',
	},
	header: {
		fontSize: 18,
		fontWeight: 'bold',
	},
	detail: {
		fontSize: 24,
	},
	red: {
		color: 'red',
	},
	green: {
		color: 'green',
	},
	inc: {
		color: 'blue',
	},
	dec: {
		color: 'red',
	},
	inputLabel: {
		paddingTop: 12,
	},
});

export default function App() {
	return (
		<NavigationContainer>
			<Tab.Navigator>
				<Tab.Screen
					name="Home"
					children={() => {
						return (
							<View style={{ padding: 10 }}>
								<Text style={{ fontSize: 24 }}>Welcome,</Text>
								<Home nama="Budi" styles={styles} />
							</View>
						);
					}}
					options={{
						tabBarLabel: 'Home',
						tabBarIcon: ({ color, size }) =>
							<MaterialCommunityIcons name="home" color={color} size={size} />,
					}}
				/>
				<Tab.Screen
					name="Category"
					children={() => {
						return (
							<View style={{ padding: 10 }}>
								<Category styles={styles} />
							</View>
						);
					}}
					options={{
						tabBarLabel: 'Category',
						tabBarIcon: ({ color, size }) =>
							<MaterialCommunityIcons name="shape" color={color} size={size} />,
					}}
				/>
			</Tab.Navigator>
		</NavigationContainer>
	);
}
